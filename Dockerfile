# This file is a template, and might need editing before it works on your project.
FROM python:3-alpine

WORKDIR /usr/src/app

RUN apk add --no-cache gcc

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

COPY . /usr/src/app

EXPOSE 5000
CMD ["python", "app.py"]

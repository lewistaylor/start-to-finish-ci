from flask import Flask
app = Flask(__name__)

from UnleashClient import UnleashClient
client = UnleashClient("https://gitlab.com/api/v4/feature_flags/unleash/15337444", "production", "PYUx1FpneRv4FSEGb1nk")
client.initialize_client()

if client.is_enabled("add-numbers"):
    @app.route('/add')
    def add():
    return {"result": "added"}
